This project demonstrates how to use the docker-privileged shared runners
for CI jobs that require access to a Docker daemon.

See [KB0005851](https://cern.service-now.com/service-portal/article.do?n=KB0005851)
for details about the docker-privileged shared runners.